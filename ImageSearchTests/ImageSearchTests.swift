//
//  ImageSearchTests.swift
//  ImageSearchTests
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import XCTest
@testable import ImageSearch

class ImageSearchTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testimagesViewModel() {
        let imageM = ImagesData(url: "http://cdn01.cdn.justjared.com/wp-content/uploads/2016/11/taylor-thanksgiving/taylor-swift-thanksgiving-04.jpg", height: 816, width: 1086, thumbnail: "https://rapidapi.contextualwebsearch.com/api/thumbnail/get?value=8597508324235810779", thumbnailHeight: 152, thumbnailWidth: 203, base64Encoding: nil, name: "taylor swift thanksgiving 043815106", title: "Taylor Swift's Thanksgiving Dinner Included Squad Members Martha Hunt & Lily Donaldson: Photo 3815106 | Andrea Swift, Austin Swift, Lily Donaldson, Martha Hunt, Scott Swift, Taylor Swift, Thanksgiving, Todrick Hall Pictures | Just Jared", imageWebSearchUrl: "https://contextualwebsearch.com/search/swift%20taylor/images")
        let imageVM = ImagesViewModel(imagesData: imageM)
        XCTAssertEqual(imageM.thumbnail, imageVM.thumbnailImage)
        XCTAssertEqual(imageM.url, imageVM.image)
    }
    
    func testwebViewModel() {
        let webDataM = WebData(title: "<b>Taylor Swift</b> Loves Her Fans! | <b>Taylor Swift</b> : Just Jared", url: "http://www.justjared.com/2012/09/15/taylor-swift-loves-her-fans/", description: "<b>Taylor Swift</b> Loves Her Fans! <b>Taylor Swift</b> snaps a pic of herself with a fan on Friday (September 14) in Rio de Janeiro, Brazil. The 22-year-old musician even signed some autographs before…", body: "Taylor Swift Loves Her Fans! Taylor Swift snaps a pic of herself with a fan on Friday (September 14) in Rio de Janeiro, Brazil. The 22-year-old musician even signed some autographs before…", keywords: "", language: "en", isSafe: true, datePublished: "2019-04-18T11:27:00")
        
        let webVM = WebViewModel(webData: webDataM)
        
        XCTAssertEqual(webDataM.url, webVM.url)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
