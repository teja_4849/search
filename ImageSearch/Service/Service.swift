//
//  Service.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

class Service: NSObject {
    static let shared = Service()
    
    func fetchImagesData(_ url: String, parameters: [String: String], completion: @escaping ([ImagesData]?, Error?) -> ()) {
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url: components.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        let headers = [
            "x-rapidapi-host": "contextualwebsearch-websearch-v1.p.rapidapi.com",
            "x-rapidapi-key": "888a948c1emsh7152fb965847a87p1bf718jsn5935be255028"
        ]
        request.allHTTPHeaderFields = headers
        let decoder = JSONDecoder()
        
        URLSession.shared.dataTask(with: request) { data, response, err in
            if let err = err {
                completion(nil, err)
                print("error in fetching Images Data: \(err )")
                return
            }
            guard let Jsondata = data else { return }
            print("yay! we got response :", String(decoding: Jsondata, as: UTF8.self))
            do {
                let imagesJson = try decoder.decode(WholeImagesData.self, from: Jsondata)
                DispatchQueue.main.async {
                    completion(imagesJson.value, nil)
                }
                
            }
            catch {
                print("the error is : \(error)")
            }
            }.resume()
        
    }
    
    func fetchWebData(_ url: String, parameters: [String: String], completion: @escaping ([WebData]?, Error?) -> ()) {
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url: components.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        let headers = [
            "x-rapidapi-host": "contextualwebsearch-websearch-v1.p.rapidapi.com",
            "x-rapidapi-key": "888a948c1emsh7152fb965847a87p1bf718jsn5935be255028"
        ]
        request.allHTTPHeaderFields = headers
        let decoder = JSONDecoder()
        
        URLSession.shared.dataTask(with: request) { data, response, err in
            if let err = err {
                completion(nil, err)
                print("error in fetching Images Data: \(err )")
                return
            }
            guard let Jsondata = data else { return }
            //print("yay! we got web response :", String(decoding: Jsondata, as: UTF8.self))
            do {
                let imagesJson = try decoder.decode(wholeWebData.self, from: Jsondata)
                DispatchQueue.main.async {
                    completion(imagesJson.value, nil)
                }
                
            }
            catch {
                print("the error is teja : \(error)")
            }
            }.resume()
        
    }
    
}
