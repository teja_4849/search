//
//  WebViewCell.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit
import WebKit

class WebCell: UICollectionViewCell {
    
    @IBOutlet weak var web: WKWebView!
    
}
