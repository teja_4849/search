//
//  ImageCache.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    static func storeImage(urlString:String, img:UIImage)  {
        let path = NSTemporaryDirectory().appending(UUID().uuidString)
        let url = URL(fileURLWithPath: path)
        let data = img.jpegData(compressionQuality: 0.5)
        
        try? data?.write(to: url)
        var dict = UserDefaults.standard.object(forKey: "imageCache") as? [String:String]
        if dict == nil {
            dict = [String:String]()
        }
        dict![urlString] = path
        UserDefaults.standard.set(dict, forKey: "imageCache")
        
        
    }
    
    static func loadImages (urlString: String, completion: @escaping (String,UIImage) -> Void ) {
        
        if let dict = UserDefaults.standard.object(forKey: "imageCache") as? [String:String] {
            if let path = dict[urlString] {
                if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                    if  let img = UIImage(data: data) {
                        completion(urlString, img)
                        return
                    }
                    
                }
            }
        }
        
        guard let url = URL(string: urlString) else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard err == nil else {return}
            guard let receivedData = data else {return}
            
            DispatchQueue.main.async {
                if let image = UIImage(data: receivedData)  {
                    
                    storeImage(urlString: urlString, img: image)
                    completion(urlString, image)
                }
            }
        }
        task.resume()
        
    }
    
    
}
