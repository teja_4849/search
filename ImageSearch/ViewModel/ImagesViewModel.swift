//
//  ImagesViewModel.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

struct ImagesViewModel {
    
    let image:String
    let thumbnailImage: String
    
    init(imagesData: ImagesData) {
        self.image = imagesData.url
        self.thumbnailImage = imagesData.thumbnail
    }
    
}

struct WebViewModel {
    
    let url:String
    
    init(webData: WebData) {
        self.url = webData.url
    }
    
}
