//
//  ImageModel.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
import UIKit

struct ImagesData: Codable {
    let url: String
    let height: Int
    let width: Int
    let thumbnail: String
    let thumbnailHeight: Int
    let thumbnailWidth: Int
    let base64Encoding: String?
    let name: String
    let title: String
    let imageWebSearchUrl: String
}

struct  WholeImagesData: Codable {
    let _type: String
    let totalCount: Int
    let value: [ImagesData]
}

struct WebData: Codable {
    let title: String
    let url: String
    let description: String
    let body: String
    let keywords: String
    let language: String
    let isSafe: Bool
    let datePublished: String
}

struct wholeWebData: Codable {
    let _type: String
    let didUMean: String
    let totalCount: Int
    let relatedSearch: [String]
    let value: [WebData]
}
