//
//  WebViewController.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/19/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation
import WebKit

class WebViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var urls: [WebViewModel] = []
    let webView = WKWebView()
    
    @IBOutlet weak var webList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("is it coming here \(urls.count)")
        return urls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "webcell", for: indexPath) as!
        WebCell
        let url = urls[indexPath.row].url
        DispatchQueue.main.async {
            cell.web.load(url)
            
        }
        return cell
    }
    
    
}

extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}
