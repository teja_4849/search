//
//  ViewController.swift
//  ImageSearch
//
//  Created by MALLOJJALA PAVAN TEJA on 3/12/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var imageViewModels = [ImagesViewModel]()
    var webViewModel = [WebViewModel]()
    var params: [String:String] = [:]
    @IBOutlet weak var imagesList: UICollectionView!
    @IBOutlet weak var webButton: UIButton!
    @IBOutlet weak var searchbtn: UIButton!
    @IBOutlet weak var searchTf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func searchAction(_ sender: Any) {
        let searchKey: String = searchTf.text ?? ""
        params["autoCorrect"] = "false"
        params["pageNumber"] = "1"
        params["pageSize"] = "10"
        params["q"] = "\(searchKey)"
        params["safeSearch"] = "false"

        fetchData(params: params)
        searchTf.text = ""

    }
    
    @IBAction func openWeb(_ sender: Any) {
        
        let searchKey: String = searchTf.text ?? ""
        params["autoCorrect"] = "true"
        params["pageNumber"] = "1"
        params["pageSize"] = "10"
        params["q"] = "\(searchKey)"
        params["safeSearch"] = "false"
        fetchWebData(params: params)
        searchTf.text = ""
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagecell", for: indexPath) as! imageCell
        let imgUrl = imageViewModels[indexPath.row].thumbnailImage
        DispatchQueue.main.async {
            ImageCache.loadImages(urlString: imgUrl) { (urlStr, img) in
                cell.tImage.image = img
                
            }
        }
      
        
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let imagesCount = imageViewModels.count
        if indexPath.row == imagesCount - 1 {
            let temp = imagesCount.roundedToNearest(multipleOf: 10)

            params["pageSize"] = "\(temp+10)"
            fetchData(params: params)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let orginalImage = storyBoard.instantiateViewController(withIdentifier: "orginalImage") as! orginalImage
        orginalImage.img = imageViewModels[indexPath.row].image
        self.navigationController?.pushViewController(orginalImage, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 16
        
        let totalSpacing = (2 * spacingBetweenCells) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)         
        if let collection = self.imagesList{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: width)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    

    fileprivate func fetchData(params: [String:String]) {
        
      let urlString = "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/ImageSearchAPI"
        Service.shared.fetchImagesData(urlString, parameters: params) { (imagesData, err) in
                        if let err = err {
                            print("Failed to fetch Images Data:", err)
                            return
                        }
            self.imageViewModels = imagesData?.map({ImagesViewModel(imagesData: $0)}) ?? []
           // print("\n\n\n in view controller : count \(self.imageViewModels.count) \n\n result \(self.imageViewModels)")
            self.imagesList.reloadData()
        }

}
    
    fileprivate func fetchWebData(params: [String:String]) {
        
        let urlString = "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/WebSearchAPI"

        Service.shared.fetchWebData(urlString, parameters: params) { (webData, err) in
            if let err = err {
                                print("Failed to fetch web Data:", err)
                                return
                            }
            self.webViewModel = webData?.map({WebViewModel(webData: $0)}) ?? []
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let webView = storyBoard.instantiateViewController(withIdentifier: "webView") as! WebViewController
            webView.urls = self.webViewModel
            self.navigationController?.pushViewController(webView, animated: true)

            
        }
    
    }


}



extension Int {
    func roundedToNearest(multipleOf m: Int) -> Int {
        let reminder = self%m
        let toadd = reminder > 5 ? +(10-reminder) : -(reminder)
        
        return self + toadd }
}
